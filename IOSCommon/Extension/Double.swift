//
//  Int.swift
//  eval
//
//  Created by hengchengfei on 15/9/6.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit

public extension Double {

    /**
      毫秒转化为日期
    */
    func toDate() -> NSDate {
        let d:NSTimeInterval = self/1000
        
        return NSDate(timeIntervalSince1970: d)
    }

}
