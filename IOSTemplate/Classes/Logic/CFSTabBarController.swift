//
//  CSFTabBarController.swift
//  csfsamradar
//
//  Created by hengchengfei on 15/7/15.
//  Copyright (c) 2015年 vsto. All rights reserved.
//

import UIKit

class CFSTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let textAttributeDefault = [NSFontAttributeName:UIFont.systemFontOfSize(11.0),NSForegroundColorAttributeName:UIColor.darkGrayColor()]
        let textAttributeSelected = [NSFontAttributeName:UIFont.systemFontOfSize(12.0),NSForegroundColorAttributeName:UIColor(netHex: kLogoColorBlue, alpha: 1.0)]
        
        //tab0
        let item0:UITabBarItem = self.tabBar.items![0] 
        item0.title = "热门"
        let item0Image = UIImage(named: "tabbar_home")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        let item0ImageSelected = UIImage(named: "tabbar_home_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item0.image = item0Image
        item0.selectedImage = item0ImageSelected
        item0.setTitleTextAttributes(textAttributeDefault, forState: UIControlState.Normal)
        item0.setTitleTextAttributes(textAttributeSelected, forState: UIControlState.Selected)
        
        //tab1
        let item1:UITabBarItem = self.tabBar.items![1] 
        item1.title = "排行"
        let item1Image = UIImage(named: "tabbar_order")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        let item1ImageSelected = UIImage(named: "tabbar_order_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item1.image = item1Image
        item1.selectedImage = item1ImageSelected
        item1.setTitleTextAttributes(textAttributeDefault, forState: UIControlState.Normal)
        item1.setTitleTextAttributes(textAttributeSelected, forState: UIControlState.Selected)
        
        //tab2
        let item2:UITabBarItem = self.tabBar.items![2] 
        item2.title = "我的"
        let item2Image = UIImage(named: "tabbar_personal")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        let item2ImageSelected = UIImage(named: "tabbar_personal_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item2.image = item2Image
        item2.selectedImage = item2ImageSelected
        item2.setTitleTextAttributes(textAttributeDefault, forState: UIControlState.Normal)
        item2.setTitleTextAttributes(textAttributeSelected, forState: UIControlState.Selected)

    }
    
 
}
